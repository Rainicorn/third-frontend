$.typeahead({
    input: '.js-typeahead-game_v1',
    minLength: 1,
    maxItem: 8,
    order: "asc",
    searchOnFocus: true,
    hint: true,
    group: true,
    maxItemPerGroup: 5,
    dropdownFilter: "all",
    source: {
        game: {
            ajax: {
                url: "http://www.gamer-hub.com/game/list.json",
                dataType: "jsonp",
                path: "data"
            }
        },
        category: {
            ajax: {
                url: "http://www.gamer-hub.com/category/list.json",
                dataType: "jsonp",
                path: "data"
            }
        },
        tag: {
            ajax: {
                url: "http://www.gamer-hub.com/tag/list.json",
                dataType: "jsonp",
                path: "data"
            }
        }
    },
    callback: {
        onClick: function (node, a, item, event) {
            window.open(
                "http://www.gamer-hub.com/" +
                    item.group + "/" +
                    item.id + "/" +
                    item.display.replace(/[\s]|:\s/g, "-")
                        .replace("'", "-")
                        .toLowerCase()
                    + "/"
            );
        }
    }

});